let num = prompt("Введите любое число от 0");

while (isNaN(+num) || num === "" || num === null) {
    num = prompt("Необходимо ввести число!");
}
while (!Number.isInteger(num)) {
    num = +prompt("Необходимо ввести целое число!");
}

if ( num < 6 ) {
    console.log("Sorry no numbers");
} else {
    for (let i = 1;  num >= i; i++ ) {
        if ( i % 5 == 0 ) {
            console.log(`Number - ${i}`)
        }
    }
}
